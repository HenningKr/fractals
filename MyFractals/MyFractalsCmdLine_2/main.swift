//
//  main.swift
//  MyFractalsCmdLine_2
//
//  Created by Henning on 03.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation

// --- (1) specify direcory and Filename for saving -------------------------------------------------------------
// --- (1.1) users home directory to save the BitMap as TIFF
let dirs : [String]? = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String]
// if there are subfolders in users home
if (dirs != nil) {
    // use the documents folder
    let directories:[String] = dirs!
    let dir = directories[0]            // users 'documents' directory
    
    // --- (1.2) Specify the FileName
    let fileName = "myMandelbrotTest1.tif"  // the filename to save
    
    // --- (2) Create the Mandelbrot image ------------------------------------------------------------------------
    // ... Creating the Mandelbrot Bitmap .............................................................

    let bitmapWidth:Int = 1024  // 800      // 384      //  800 // 800 = 4*200
    let bitmapHeight:Int = 1024     // 800     // 384     // 500 //    = 2.5 * 200              800
    
    let minX:Double = -1.0      // -2.0          // 2.1           // -2.0
    let maxX:Double = 2.5       // 0.5           // -0.6          // 2.0 // 0.5
    let minY:Double = -1.5      // -1.25          // -1.35         // -1.25
    let maxY:Double = 1.5 // 1.25          // 1.35          // 1.25
    
    let maxSquareValue:Double = 144.0    // 4.0
    let maxIter:Int = 1000             // 32
    
    // let myMandelbrot:MyMandelbrot = MyMandelbrot()
    // let mandelbrotBitMap:MyBitMap = myMandelbrot.calcMandelbrotBitMap_Single( minX, maxX:maxX, zoomFactorX: zoomX, minY:minY, maxY:maxY, zoomFactorY:zoomY, maxSquareValue:maxSquareValue, maxIter:maxIter)
  
    let stoppWatch:StoppWatch = StoppWatch()
    stoppWatch.start()
    let myMandelbrot:MyMandelbrot = MyMandelbrot( bitmapWidth: bitmapWidth, bitmapHeight: bitmapHeight, minX:minX, maxX:maxX, minY:minY, maxY:maxY, maxSquareValue:maxSquareValue, maxIter:maxIter)
    let mandelbrotBitMap:MyBitMap = myMandelbrot.calcMandelbrotBitMap_Single()
    stoppWatch.stop()
    
    // ... Saving the Bitmap .............................................................
    mandelbrotBitMap.saveBitMapAsTIFF( dir, toFileName: fileName )
    
    print("Finished creating and writing Mandelbrot Image to file \(fileName) in path: \(dir) to users home.\n\(myMandelbrot)\nTime need for execution: \(stoppWatch.execTime) => \(stoppWatch.formattedExecTime_DAYS_HH_MM_SS_MILISEC)")
    print("Ready.")
    
} else {
    
    /// @ToDo: Error Handling
    print( "@ERROR: at MyMandelbrot.saveBitMapAsTIFF() user home does not have any diercories.")
}
