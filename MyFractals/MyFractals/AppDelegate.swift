//
//  AppDelegate.swift
//  MyFractals
//
//  Created by Henning on 31.03.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

