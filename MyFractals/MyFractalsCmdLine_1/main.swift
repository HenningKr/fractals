//
//  main.swift
//  MyFractalsCmdLine_1
//
//  Created by Henning on 31.03.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation

// --- (1) specify direcory and Filename for saving -------------------------------------------------------------
// --- (1.1) users home directory to save the BitMap as TIFF
let dirs : [String]? = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String]
// if there are subfolders in users home
if (dirs != nil) {
    // use the documents folder
    let directories:[String] = dirs!
    let dir = directories[0]            // users 'documents' directory
    
    // --- (1.2) Specify the FileName
    let fileName = "myBitMapTest1.tif"  // the filename to save

    // --- (2) Do the BitMap work -------------------------------------------------------------------------------
    // ... Creating the Bitmap .............................................................
    let width:Int = 800                                // width of the BitMap
    let height:Int = 800                               // height of the BitMap
    let myBitMap:MyBitMap = MyBitMap( width, height)    // create the BitMap

    for var x:Int = 0; x < width; ++x {                // draw the pixels
        for var y:Int = 0; y < height; ++y {
            let r:UInt8 = UInt8((x*y) % 255)
            let g:UInt8 = UInt8((x+y) % 255)
            let b:UInt8 = UInt8( ( (x / ( y != 0 ? y : 1) ) * 1024) % 255)
            myBitMap.setPixel( x, atY: y, red: r, green: g, blue: b, alpha: 255)
        }
    }

    // ... Saving the Bitmap .............................................................
    myBitMap.saveBitMapAsTIFF( dir, toFileName: fileName )

    print("Finished creating and writing to file \(fileName) in path: \(dir) to users home.")
    print("Ready.")

} else {

    /// @ToDo: Error Handling
    print( "@ERROR: at MyBitMap.saveBitMapAsTIFF() user home does not have any diercories.")
}
