//
//  StoppWatchTests.swift
//  MyFractals
//
//  Created by Henning on 04.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Cocoa
import XCTest

class StoppWatchTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    /// -----------------------------------------------------
    /// Simple test starting and stopping the StoppWatch
    func testSoppWatch_StartStop() {
        
        //
        // let stoppWatch = StoppWatch( startValue, stopValue)
        let stoppWatch = StoppWatch()
        stoppWatch.start()
        // for var i = 1000; i > 0; i-- {
        for _ in 1000...1 {
            // just run a loop
        }
        stoppWatch.stop()
        
        // Some Value > 0 has been stopped.
        XCTAssert( stoppWatch.execTime! > 0.0, "Passed: \(stoppWatch.execTime ?? 0.0) > 0.0")
    }
    
    /// -----------------------------------------------------
    /// Simple test starting and stopping the StoppWatch twice
    func testSoppWatch_StartStopStop() {
        
        //
        // let stoppWatch = StoppWatch( startValue, stopValue)
        let stoppWatch = StoppWatch()
        stoppWatch.start()
        // for var i = 1000; i > 0; i-- {
        for _ in 1000...1 {
            // just run a loop
        }
        stoppWatch.stop()
        let firstStop = stoppWatch.execTime
        
        // for var i = 1000; i > 0; i-- {
        for _ in 1000...1 {
            // just run a loop
        }
        stoppWatch.stop()
        let secondStop = stoppWatch.execTime

        
        // Some Value > 0 has been stopped.
        XCTAssert( firstStop! > 0.0, "Passed: \(String(describing: firstStop)) > 0.0")
        XCTAssert( secondStop! > firstStop!, "Passed: \(String(describing: firstStop)) < \(String(describing: secondStop))")
    
    }
    
    /// -----------------------------------------------------
    /// test formatting
    func testSoppWatch_TimeTest() {
        
        // init values
        let start_milliseconds = 1234
        let start_seconds = 13
        let start_minutes = 34
        let start_hours = 14
        let start_days = 3
        
        let end_milliseconds = 6789
        let end_seconds = 37
        let end_minutes = 48
        let end_hours = 17
        let end_days = 5
        
        // expected values
        let diff_milliseconds = end_milliseconds - start_milliseconds            // 5555
        let diff_seconds = end_seconds - start_seconds                          // 24
        let diff_minutes = end_minutes - start_minutes                          // 18
        let diff_hours = end_hours - start_hours                                // 3
        let diff_days = end_days - start_days                                   // 2
        
        // init values
        // let startTime:Double = (((((start_days*24) + start_hours) * 60) + start_minutes) * 60 ) + start_seconds + start_milliseconds/1000
        var startTime:Double = Double(start_days*24)
        startTime += Double(start_hours)
        startTime *= 60.0
        startTime += Double(start_minutes)
        startTime *= 60.0
        startTime += Double(start_seconds)
        startTime += Double(start_milliseconds)/10000
        
        // let endTime:Double = (((((end_days*24) + end_hours) * 60) + end_minutes) * 60 ) + end_seconds + end_milliseconds/1000
        var endTime:Double = Double( end_days*24)
        endTime += Double( end_hours)
        endTime *= 60.0
        endTime += Double( end_minutes)
        endTime *= 60.0
        endTime += Double( end_seconds)
        endTime += Double( end_milliseconds) / 10000
        
        
        let diffTime = endTime - startTime
        
        // let stoppWatch = StoppWatch( startValue, stopValue)
        let stoppWatch = StoppWatch( startTime: startTime, endTime: endTime)
        
        XCTAssert( stoppWatch.execTime! > 0.0, "Passed: \(String(describing: stoppWatch.execTime)) > 0.0")
        XCTAssert( stoppWatch.execTime == diffTime, "Passed: \(String(describing: stoppWatch.execTime)) == \(diffTime)")
        
        XCTAssert( stoppWatch.execTime_MillisecondsUInt() == UInt(diff_milliseconds), "Passed: \(stoppWatch.execTime_MillisecondsUInt()) == \(diff_milliseconds)")
        XCTAssert( stoppWatch.execTime_SS == UInt(diff_seconds), "Passed: \(stoppWatch.execTime_SS) == \(diff_seconds)")
        XCTAssert( stoppWatch.execTime_MM == UInt(diff_minutes), "Passed: \(stoppWatch.execTime_MM) == \(diff_minutes)")
        XCTAssert( stoppWatch.execTime_HHH == UInt(diff_hours) + UInt(24*diff_days), "Passed: \(stoppWatch.execTime_HHH) == \(diff_hours + 24*diff_days)")
        XCTAssert( stoppWatch.execTime_HH == UInt(diff_hours), "Passed: \(stoppWatch.execTime_HH) == \(diff_hours)")
        XCTAssert( stoppWatch.execTime_DAYS == UInt(diff_days), "Passed: \(stoppWatch.execTime_DAYS) == \(diff_days)")
    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure() {
            // Put the code you want to measure the time of here.
        }
    }

}
