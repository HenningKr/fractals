//
//  MyBitMapSwift_GetColor_Tests.swift
//  MyFractals
//
//  Created by Henning on 01.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Cocoa
import XCTest

class MyBitMapSwift_GetColor_Tests: XCTestCase {

    // --- MyBitMapObject --------------------------------------------------------------------------------------
    let myBitMap = MyBitMap( 100, 100)
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    /// ---------------------------------------------------------------------------------------------------------
    /// Test scope: getColor() method from MyBitMap class.
    /// Systematically test with the full color set.
    /// (Remark: takes some execution time)
    func testMyBitMap_GetColor_FullColorSet() {
        var theValue:UInt = 0
        // for var r:UInt = 0; r <= 255; ++r {                                                     // for any red
        //    for var g:UInt = 0; g <= 255; ++g {                                                 // for any green
        //        for var b:UInt = 0; b <= 255; ++b {
        for var r:UInt in 0...255 {                                                     // for any red
        for var g:UInt in 0...255 {                                                 // for any green
            for var b:UInt in 0...255 {                                             // for any blue
                    theValue = b + g*256 + r*256*256                                            // the color value
                    // let ( red:UInt8, green:UInt8, blue:UInt8 ) = myBitMap.getColor( theValue)   // get the (r,g,b) values for the color value    /// @Version Swift 1.x
                let ( red, green, blue ): (UInt8,UInt8,UInt8) = myBitMap.getColor( rgbValue: theValue)   // get the (r,g,b) values for the color value    /// @Version Swift 2
                    XCTAssert( UInt8(r) == red, "Success: r:\(r) == red:\(red)")
                    XCTAssert( UInt8(g) == green, "Success: g:\(g) == green:\(green)")
                    XCTAssert( UInt8(b) == blue, "Success: b:\(b) == blue:\(blue)")
                }
            }
        }
    }
    
    /// ---------------------------------------------------------------------------------------------------------
    /// Test scope: getColor() method from MyBitMap class.
    /// Test numbers that are higher than the lower bytes that specify the (r,g,b)
    func testMyBitMap_GetColor_BigInt() {
        
        let r:UInt = 12                                                             // red value
        let g:UInt = 13                                                             // green value
        let b:UInt = 14                                                             // blue value
        let bigInt:UInt = 123*256*256*256 + b + g*256 + r*256*256                   // there is a part on the high bytes above the r,g,b values
        // let ( red:UInt8, green:UInt8, blue:UInt8 ) = myBitMap.getColor( bigInt)     // get the r,g,b for teh color value
        let ( red, green, blue ):(UInt8, UInt8, UInt8) = myBitMap.getColor( rgbValue: bigInt)     // get the r,g,b for teh color value
        
        XCTAssert( UInt8(r) == red, "Success: r:\(r) == red:\(red)")
        XCTAssert( UInt8(g) == green, "Success: g:\(g) == green:\(green)")
        XCTAssert( UInt8(b) == blue, "Success: b:\(b) == blue:\(blue)")
        
    }

    /// ---------------------------------------------------------------------------------------------------------
    /// Test scope: getColor() method from MyBitMap class.
    /// Test numbers that cover only the lower bytes that specify the (r,g,b)
    func testMyBitMap_GetColor_Int() {
        
        let r:UInt = 12                                                             // red value
        let g:UInt = 13                                                             // green value
        let b:UInt = 14                                                             // blue value
        let rgbInt:UInt = b + g*256 + r*256*256                                     // there is a part on the high bytes above the r,g,b values
        // let ( red:UInt8, green:UInt8, blue:UInt8 ) = myBitMap.getColor( rgbInt)     // get the r,g,b for teh color value
        let ( red, green, blue ):(UInt8, UInt8, UInt8) = myBitMap.getColor( rgbValue: rgbInt)     // get the r,g,b for teh color value
        
        XCTAssert( UInt8(r) == red, "Success: r:\(r) == red:\(red)")
        XCTAssert( UInt8(g) == green, "Success: g:\(g) == green:\(green)")
        XCTAssert( UInt8(b) == blue, "Success: b:\(b) == blue:\(blue)")
        
    }

    /// ---------------------------------------------------------------------------------------------------------
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure() {
            // Put the code you want to measure the time of here.
        }
    }

}
