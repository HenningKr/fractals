//
//  MyBitMap.swift
//  MyFractals
//
//  Created by Henning on 31.03.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation
import Cocoa

// see more about thread safe design at
// https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/Multithreading/ThreadSafety/ThreadSafety.html
// NSLock* arrayLock = GetArrayLock();
// NSMutableArray* myArray = GetSharedArray();
// id anObject;

// [arrayLock lock];
// anObject = [myArray objectAtIndex:0];
// [arrayLock unlock];

// [anObject doSomething];

class MyBitMap {
    
    /// --- Rectangle that defines the Bitmap dimensions (x_min, y_min),(x_max, y_max) ------------
    var xMin:Int = 0
    var yMin:Int = 0
    var xMax:Int = 0
    var yMax:Int = 0
   
    /// --- The BitMap array ----------------------------------------------------------------------
    /// @arrayLock
    let dataArrayLock:NSLocking = NSLock()
    
    /// @data           - the BitMap array
    var data:[UInt8] = [UInt8]()
    
    /// --- Calculated properties -----------------------------------------------------------------
    /// @width          - the width of the Bitmap
    var width: Int {
        get { return xMax - xMin + 1 }
        set { xMax = xMin + newValue - 1 }
    }
    
    /// @height         - the height of the BitMap
    var height: Int {
        get { return yMax - yMin + 1}
        set { yMax = yMin + newValue - 1 }
    }
    
    /// @dataLength     - the size of BitMap
    var dataLength:Int {
        get { return width * height * 4 }
    }


    /// Initializer
    init( _ xMin: Int, _ yMin: Int, _ xMax: Int, _ yMax: Int) {

        // set the class attributes
        self.xMin = xMin
        self.yMin = yMin
        self.xMax = xMax
        self.yMax = yMax
        
        // reserve the BitMap array capacity
        data.reserveCapacity( dataLength)
        // self.data = [UInt8]( unsafeUninitializedCapacity: dataLength, initializingWith: UInt8(0))
        self.data = Array( repeating: UInt8(0), count: dataLength)

        // println("set capacity to dat length \(dataLength) the capacity is now \(data.capacity) with startIndex \(data.startIndex) and endIndex \(data.endIndex)")
    }
    
    /// Convenience Initializer
    convenience init( _ with: Int, _ height: Int) {
        self.init( 0, 0, with-1, height-1)
    }
    
    
    /// @func setPixel
    func setPixel( atX: Int, atY: Int, rgb: UInt, alpha a: UInt8 = 255) {
        let (r, g, b): (UInt8, UInt8, UInt8) = getColor( rgbValue: rgb)
        setPixel( atX: atX, atY:atY, red:r, green:g, blue:b, alpha:a)
    }
    
    /// @func setPixel
    func setPixel( atX: Int, atY: Int, red r: UInt8, green g: UInt8, blue b: UInt8, alpha a: UInt8 = 255) {
        
        if ( atX >= xMin && atX < xMax && atY >= yMin && atY < yMax) {
            // Pixel Position in the BitMap
            let index_r:Int = 4 * (( atX - self.xMin) + (atY - self.yMin)  * width)

            // add pixel values into the BitMap
            data[index_r] = r
            data[index_r+1] = g
            data[index_r+2] = b
            data[index_r+3] = a
        }
        
    }
    
    // finally copies the sub area into the Bitmap
    func copyData( offsetBitMapX:Int, dataToCopy:[UInt8], bitmapWidth:Int, bitmapHeight:Int) {
        
        dataArrayLock.lock()
        
        // for var y:Int = 0; y < bitmapHeight; ++y {
            
        //    for var x:Int = 0; x < bitmapWidth; ++x {

        for y:Int in 0...bitmapHeight-1 {
            
            for x:Int in 0...bitmapWidth-1 {

                let sourceIndex_r:Int = 4 * (x + y * bitmapWidth)
                let targetIndex_r:Int = 4 * (x + y * self.width + offsetBitMapX)
                
                data[ targetIndex_r ]       = dataToCopy[ sourceIndex_r ]
                data[ targetIndex_r + 1 ]   = dataToCopy[ sourceIndex_r + 1 ]
                data[ targetIndex_r + 2]    = dataToCopy[ sourceIndex_r + 2]
                data[ targetIndex_r + 3]    = dataToCopy[ sourceIndex_r + 3]

            }
        }
        
        
        dataArrayLock.unlock()

    }


    /// @func get_color
    /// transforms an UInt value into its r,g,b components
    func getColor( rgbValue:UInt) -> (r:UInt8, g:UInt8, b:UInt8) {
        let blue:UInt8 = UInt8( rgbValue & 255)
        let green:UInt8 = UInt8( (rgbValue >> 8) & 255)
        let red:UInt8 = UInt8( (rgbValue >> 16) & 255)
        return ( red, green, blue)
    }
    
    /// @func getNSImage
    /// @returns NSImage 
    /// Create the NSImage for the BitMap
    func getNSImage() -> NSImage {
        // Create a CGImage with the pixel data
        // CGDataProviderRef CGDataProviderCreateWithData(void *info, const void *data, size_t size, CGDataProviderReleaseDataCallback releaseData);
        // let provider:CGDataProvider  = CGDataProviderCreateWithData( nil, self.data, self.dataLength, nil)!
        let provider:CGDataProvider  = CGDataProviderCreateWithData( nil, self.data, self.dataLength, nil)!
        let colorspace:CGColorSpace  = CGColorSpaceCreateDeviceRGB()
        let bitsPerComponent:Int = 8
        let bitsPerPixel:Int = 32
        let bytesPerRow:Int = self.width * 4
        let bitmapInfo:CGBitmapInfo = CGBitmapInfo( rawValue: UInt32(CGBitmapInfo.byteOrder32Big.rawValue|CGImageAlphaInfo.PremultipliedLast.rawValue))
        let image:CGImage = CGImageCreate( width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorspace, bitmapInfo, provider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)!
        var size:NSSize = NSSize()
        size.width = CGFloat( self.width)
        size.height = CGFloat( self.height)
        return NSImage( cgImage: image, size: size)
    }
    
    /// @func TIFFRepresentation
    /// @returns NSData
    /// Create the TIFF representation of the image
    func TIFFRepresentation() -> NSData? {
        let img: NSImage = self.getNSImage()
        return img.tiffRepresentation as NSData?
        // return tiffRepresentation.TIFFRepresentation
    }
    
    /// @func saveBitMapAsTIFF()
    /// Writes a BitMap in TIFF format to the documents folder with the given 'toFileName' of the user
    func saveBitMapAsTIFF( atDir:String, toFileName:String) {
        // add the filename
        
        // let tiffPath = atDir.stringByAppendingPathComponent(toFileName)  /// @Version Swift 1.x. Doesn'T work with SWIFT 2 anymore
        
        let atUrl:NSURL = NSURL.fileURL(withPath: atDir) as NSURL                     /// @Version Swift 2
        // guard let tiffURL = atUrl.appendingPathComponent( toFileName) else { return }       /// @Version Swift 2
        guard let tiffURL = atUrl.appendingPathComponent( toFileName) else { return}       /// @Version Swift 2

        let imageData = self.TIFFRepresentation()
            
        // if ...
        // ... imageData!.writeToFile( tiffPath, options: NSDataWritingOptions.DataWritingAtomic)   /// @Version Swift 1.x. Doesn'T work with SWIFT 2 anymore
        // else 
        // ....
        
        var error: NSError?                                                                                                                 /// @Version Swift 2
        do {                                                                                                                                /// @Version Swift 2
            try imageData!.write( to: tiffURL, options: NSData.WritingOptions.atomic)                                            /// @Version Swift 2
        } catch let error1 as NSError {                                                                                                     /// @Version Swift 2
            error = error1                                                                                                                  /// @Version Swift 2
            print("@ERROR: at MyBitMap.saveBitMapAsTIFF() - \tPath = \(atDir) \tFileName = \(toFileName).\nError writing file: \(String(describing: error))")   /// @Version Swift 2
            return                                                                                                                          /// @Version Swift 2
        }                                                                                                                                   /// @Version Swift 2
    }
}


// version that allows concurrent setting of pixels
class MyBitMapConcurrent : MyBitMap {
    
    /// @func setPixel for conciurrent useConcurrently
    override func setPixel( atX: Int, atY: Int, red r: UInt8, green g: UInt8, blue b: UInt8, alpha a: UInt8 = 255) {
        
        if ( atX >= xMin && atX < xMax && atY >= yMin && atY < yMax) {
            
            // lock the data array for writing
            dataArrayLock.lock()

            super.setPixel( atX: atX, atY: atY, red: r, green: g, blue: b, alpha: a)
            
            // unlock the data array after writing
            dataArrayLock.unlock()
        }
        
    }

}
