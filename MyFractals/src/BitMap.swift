//
//  BitMap.swift
//  MyFractals
//
//  Created by Henning on 17.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation
import Cocoa



/// @class BitMap
/// Manges a BitMap
class BitMap {
    
    /// --- Rectangle that defines the Bitmap dimensions (x_min, y_min),(x_max, y_max) ------------
    var xMin:Int = 0 {
        
        didSet {
            self.width = self.xMax - self.xMin + 1      // update the width
            self.dataLength = self.width * self.height * 4
        }
    }
    
    var yMin:Int = 0 {
        
        didSet {
            self.height = self.yMax - self.yMin + 1     // update the height
            self.dataLength = self.width * self.height * 4
        }
    }
    
    var xMax:Int = 0 {
 
        didSet {
            self.width = xMax - self.xMin + 1     // update the width
            self.dataLength = self.width * self.height * 4
        }
    }
    
    var yMax:Int = 0 {
        
        didSet {
            self.height = self.yMax - self.yMin + 1    // update the height
            self.dataLength = self.width * self.height * 4
        }
    }
    
    /// --- The BitMap array ----------------------------------------------------------------------
    /// @arrayLock
    let dataArrayLock:NSLocking = NSLock()
    
    /// @data           - the BitMap array
    var data:[UInt8] = [UInt8]()
    
    /// --- Calculated properties -----------------------------------------------------------------
    /// @width          - the width of the Bitmap
    var width: Int = 0
    
    /// @height         - the height of the BitMap
    var height: Int = 0
    
    /// @dataLength     - the size of BitMap
    var dataLength:Int = 0
    
    /// Initializer
    init( _ xMin: Int, _ yMin: Int, _ xMax: Int, _ yMax: Int) {
        
        // set the class attributes
        self.xMin = xMin
        self.yMin = yMin
        self.xMax = xMax
        self.yMax = yMax
        
        self.width = xMax - xMin + 1
        self.height = yMax - yMin + 1
        self.dataLength = self.width * self.height * 4
        
        // reserve the BitMap array capacity
        data.reserveCapacity( dataLength)
        self.data = [UInt8]( unsafeUninitializedCapacity: dataLength, initializingWith: UInt8(0))
        
        // println("set capacity to dat length \(dataLength) the capacity is now \(data.capacity) with startIndex \(data.startIndex) and endIndex \(data.endIndex)")
    }
    
    /// Convenience Initializer
    convenience init( _ with: Int, _ height: Int) {
        self.init( 0, 0, with-1, height-1)
    }
    
    
    /// @func setPixel
    func setPixel( atX: Int, atY: Int, rgb: UInt, alpha a: UInt8 = 255) {
        let (r, g, b): (UInt8, UInt8, UInt8) = getColor( rgbValue: rgb)
        setPixel( atX: atX, atY:atY, red:r, green:g, blue:b, alpha:a)
    }
    
    /// @func setPixel
    func setPixel( atX: Int, atY: Int, red r: UInt8, green g: UInt8, blue b: UInt8, alpha a: UInt8 = 255) {
        
        // if ( atX >= xMin && atX < xMax && atY >= yMin && atY < yMax) {
        if ( atX >= xMin && atX <= xMax && atY >= yMin && atY <= yMax) {
            // Pixel Position in the BitMap
            let index_r:Int = 4 * (( atX - self.xMin) + (atY - self.yMin)  * width)
            
            // add pixel values into the BitMap
            data[index_r] = r
            data[index_r+1] = g
            data[index_r+2] = b
            data[index_r+3] = a

        }
    }
    
    // *** New Version: Copy Data for horizontal sections requireing just one offest for the full block instead vertical section that would require offsetting each line  ***
    // finally copies the sub area into the Bitmap
    func copyData( dataToCopy:[UInt8], bitmapWidth:Int, bitmapHeight:Int, offsetBitMapY:Int = 0) {
        
        dataArrayLock.lock()
        
        let sourceStartIndex = 0
        let targetStartIndex = 4 * (offsetBitMapY) * (bitmapWidth)
        let copyWidth = 4 * (bitmapHeight) * (bitmapWidth)                          // dataToCopy.count            // 4 * bitmapHeight * bitmapWidth
            
        if ((sourceStartIndex + copyWidth) <= dataToCopy.count) && ((targetStartIndex + copyWidth) <= data.count) {
            data[ targetStartIndex...targetStartIndex+copyWidth-1] = dataToCopy[ sourceStartIndex...sourceStartIndex+copyWidth-1]
        } else {
                // data[ targetStartIndex...targetStartIndex+(dataToCopy.count - targetStartIndex)] = dataToCopy[ sourceStartIndex...sourceStartIndex+(dataToCopy.count - targetStartIndex)]
                let copyTartgetWidth = data.count - targetStartIndex
                let copyWidth = (copyTartgetWidth <= dataToCopy.count ? copyTartgetWidth : dataToCopy.count)
                data[ targetStartIndex...targetStartIndex+copyWidth-1] = dataToCopy[ sourceStartIndex...sourceStartIndex+copyWidth-1]
        }
        dataArrayLock.unlock()
    }

    
    /// @func get_color
    /// transforms an UInt value into its r,g,b components
    func getColor( rgbValue:UInt) -> (r:UInt8, g:UInt8, b:UInt8) {
        let blue:UInt8 = UInt8( rgbValue & 255)
        let green:UInt8 = UInt8( (rgbValue >> 8) & 255)
        let red:UInt8 = UInt8( (rgbValue >> 16) & 255)
        return ( red, green, blue)
    }
    
    /// @func getNSImage
    /// @returns NSImage
    /// Create the NSImage for the BitMap
    func getNSImage() -> NSImage {
        // Create a CGImage with the pixel data
        let provider:CGDataProvider  = CGDataProviderCreateWithData( nil, self.data, self.dataLength, nil)!
        let colorspace:CGColorSpace  = CGColorSpaceCreateDeviceRGB()
        let bitsPerComponent:Int = 8
        let bitsPerPixel:Int = 32
        let bytesPerRow:Int = self.width * 4
        let bitmapInfo:CGBitmapInfo = CGBitmapInfo( rawValue: UInt32(CGBitmapInfo.byteOrder32Big.rawValue|CGImageAlphaInfo.PremultipliedLast.rawValue))
        let image:CGImage = CGImageCreate( width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorspace, bitmapInfo, provider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)!
        var size:NSSize = NSSize()
        size.width = CGFloat( self.width)
        size.height = CGFloat( self.height)
        return NSImage( cgImage: image, size: size)
    }
    
    /// @func TIFFRepresentation
    /// @returns NSData
    /// Create the TIFF representation of the image
    func TIFFRepresentation() -> NSData? {
        let img: NSImage = self.getNSImage()
        return img.tiffRepresentation as NSData?
    }
    
    /// @func aveBitMapAsTIFF()
    /// Writes a BitMap in TIFF format to the documents folder with the given 'toFileName' of the user
    func saveBitMapAsTIFF( atDir:String, toFileName:String) {
        // add the filename
        
        // let tiffPath = atDir.stringByAppendingPathComponent(toFileName)      /// @Version Swift 1.x
        
        let atUrl:NSURL = NSURL.fileURL( withPath: atDir) as NSURL                         /// @Version Swift 2
        let tiffUrl:NSURL = atUrl.appendingPathComponent( toFileName)! as NSURL      /// @Version Swift 2
        let tiffPath: String = tiffUrl.path!                                    /// @Version Swift 2
        
        let imageData = self.TIFFRepresentation()
        
        var error: NSError?
        do {
            try imageData!.write( toFile: tiffPath, options: NSData.WritingOptions.atomic)
        } catch let error1 as NSError {
            error = error1
            print("@ERROR: at MyBitMap.saveBitMapAsTIFF() - \tPath = \(atDir) \tFileName = \(toFileName).\nError writing file: \(String(describing: error))")
            return
        }
    }
}

// version that allows concurrent setting of pixels
class BitMapConcurrent : BitMap {
    
    /// @func setPixel for conciurrent useConcurrently
    override func setPixel( atX: Int, atY: Int, red r: UInt8, green g: UInt8, blue b: UInt8, alpha a: UInt8 = 255) {
        
        // if ( atX >= xMin && atX < xMax && atY >= yMin && atY < yMax) {
        if ( atX >= xMin && atX <= xMax && atY >= yMin && atY <= yMax) {
            
            // lock the data array for writing
            dataArrayLock.lock()
            
            super.setPixel( atX: atX, atY: atY, red: r, green: g, blue: b, alpha: a)
            
            // unlock the data array after writing
            dataArrayLock.unlock()
        }
        
    }
    
}
