//
//  Mandelbrot.swift
//  MyFractals
//
//  Created by Henning on 17.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation

/// #######################################################################################################
/// Mandelbrot

class Mandelbrot {
    
    // --- Class Attributes -----------------------------------------------------------------------------------
    var minX:Double = 0.0 {

        didSet {
            self.width = self.maxX - self.minX      // uodate width
        }
    }
    
    
    var maxX:Double = 0.0 {
        
        didSet {
            self.width = self.maxX - self.minX      // update width
        }
    }
    
    var minY:Double = 0.0 {
        
        didSet {
            self.height = self.maxY - self.minY     // update height
        }
        
    }
    
    var maxY:Double = 0.0 {
        
        didSet {
            self.height = self.maxY - self.minY     // update height
        }
        
    }
    
    var maxSquareValue:Double = 4.0                 // 9.0
    var maxIter:Int = 32
    
    var bitmapWidth:Int = 800 {
        
        didSet {
            self.zoomFactorX = ( self.bitmapWidth != 0 ? self.width / Double( self.bitmapWidth) : 0.0)           // update the zoomfactor
            self.dataLength = Int( self.bitmapWidth * self.bitmapHeight * 4)    // update the data array length
        }
        
    }
    
    var bitmapHeight:Int = 800 {
        
        didSet {
            self.zoomFactorY = ( self.bitmapHeight != 0 ? self.height / Double( self.bitmapHeight) : 0.0)          // update the zoomfactor
            self.dataLength = Int( self.bitmapWidth * self.bitmapHeight * 4)     // update the data array length
        }
        
    }
    
    var mandelbrotBitMap:BitMap? = nil            // the MyBitMap object
    var strCommentary:String = String()
    
    // --- Computed Properties --------------------------------------------------------------------------------
    var width:Double = 0.0 {
        
        didSet {
            self.zoomFactorX = self.width / Double( self.bitmapWidth)
        }
        
    }
    
    var height:Double = 0.0  {
        
        didSet {
            self.zoomFactorY = self.height / Double( self.bitmapHeight)
        }
    }
    
    var zoomFactorX:Double = 0.0
    var zoomFactorY:Double = 0.0
    
    var dataLength:Int = 0
    
    var description:String {
        get { return "Mandelbrot set with following parameters:\n\n\t(bitmapWidth,bitmapHeight)=(\(bitmapWidth),\(bitmapHeight))\t((minX,minY),(maxX,maxY))=((\(minX), \(minY)),(\(maxX), \(maxY)))\n\tZoom Factor X: \(zoomFactorX)\tZoom Factor Y: \(zoomFactorY)\n\tmaximum square vale: \(maxSquareValue)\n\tmaximum iterations:\(maxIter)\n\(strCommentary)" }
    }

    
    /// @syncLock
    let syncLock:NSLock = NSLock()

    
    /// --- Constructor ---------------------------------------------------------------------------------------
    
    init( bitmapWidth:Int = 800, bitmapHeight:Int = 800, minX:Double = -2.0, maxX:Double = 0.5, minY:Double = -1.25, maxY:Double = 1.25, maxSquareValue:Double = 4.0, maxIter:Int = 32) {
        
        self.bitmapWidth = bitmapWidth
        self.bitmapHeight = bitmapHeight
        
        self.minX = minX
        self.maxX = maxX
        
        self.minY = minY
        self.maxY = maxY
        
        self.maxSquareValue = maxSquareValue
        self.maxIter = maxIter

        self.width = self.maxX - self.minX
        self.height = self.maxY - self.minY
        
        self.zoomFactorX = self.width / Double( self.bitmapWidth)
        self.zoomFactorY = self.height / Double( self.bitmapHeight)
        
        self.dataLength = Int( self.bitmapWidth * self.bitmapHeight * 4)
        
    }
    
    /// -------------------------------------------------------------------------------------------------------
    /// function to calculate mandelbrot value for one oixel
    func mandelbrotPointIteration( cx: Double, cy: Double, maxSquareValue:Double, maxIter: Int) -> Int {
        
        var iter:Int = 0
        var x:Double = 0.0
        var y:Double = 0.0
        
        var xSquare:Double = 0.0
        var ySquare:Double = 0.0
        
        while  ( (xSquare + ySquare) <= maxSquareValue ) && ( iter <= maxIter ) {
            let xTemp:Double = xSquare - (ySquare + cx)
            let yTemp:Double = 2 * x * y + cy
            x = xTemp
            y = yTemp
            xSquare = x*x
            ySquare = y*y
            iter += 1
        }
        
        return iter
        
        // return( iter – log(log(betrag_quadrat) / log(4)) / log(2)) ... would return Double
        // alternative for a continous colour flow:
        // punkt_iteration = iter – log(log(betrag_quadrat) / log(4)) / log(2)
    }
    

    /// -------------------------------------------------------------------------------------------------------
    ///
    func calcMandelbrotBitMap_Section( startAt: Int, endAt:Int) {
        
        // --- custom operation work is done here. ----
        // ... for the boundaries of the section do the Mandelbrot algorithm
        // for var x:UInt = 0; x < bitmapWidth; x++ {
        // for var y:Int = 0; y <= bitmapHeight; ++y {
        for y:Int in 0...bitmapHeight {

            let cy:Double = Double( minY) + Double(y) * zoomFactorY
            
            // for var x:Int = startAt; x <= endAt; ++x {
                for x:Int in startAt...endAt {

                let cx:Double = Double( minX) + Double( x) * zoomFactorX
                
                    let valueIteration = mandelbrotPointIteration( cx: cx, cy: cy, maxSquareValue: maxSquareValue, maxIter: maxIter) * 0x09091A
                    mandelbrotBitMap!.setPixel( atX: x, atY: y, rgb:UInt(valueIteration), alpha: UInt8(255))
                
                /*
                let valueIteration = mandelbrotPointIteration( cx, cy: cy, maxSquareValue: maxSquareValue, maxIter: maxIter)
                
                var r:UInt8 = 0
                var g:UInt8 = 0
                var b:UInt8 = 0
                let alpha:UInt8 = 255
                
                if valueIteration <= maxIter {
                    if valueIteration<16 {
                        r = UInt8( valueIteration*8 )
                        g = UInt8( valueIteration*8 )
                        b = UInt8( 128+valueIteration*4 )
                    } else if valueIteration>=16 && valueIteration<64 {
                        r = UInt8( 128+valueIteration-16 )
                        g = UInt8( 128+valueIteration-16 )
                        b = UInt8( 192+valueIteration-16 )
                    } else if valueIteration>=64 {
                        r = UInt8( maxIter-valueIteration )
                        g = UInt8( 128+(maxIter-valueIteration)/2 )
                        b = UInt8( maxIter-valueIteration )
                    }
                
                    mandelbrotBitMap!.setPixel( x, atY: y, red: r, green: g, blue: b, alpha: UInt8(255))
                
                } // else color black, i.e. alraedy preset
                */
                
                
            }
        }
        // ---------------------------------------------
    }

    
    
    /// -------------------------------------------------------------------------------------------------------
    /// method to calculate the Mandelbrot BitMap without any concurrency
    func calcMandelbrotBitMap_Single() -> BitMap {
        
        // the bitmap initialization
        // ATTENTION !!! not the MyBitmap class with concurrent pixel setting
        mandelbrotBitMap = BitMap( bitmapWidth, bitmapHeight)
        
        // --- custom operation work is done here. ----
        // ... for the boundaries of the section do the Mandelbrot algorithm
        // for var x:UInt = 0; x < bitmapWidth; x++ {
        // for var y:Int = 0; y < bitmapHeight; ++y {
        for y:Int in 0...bitmapHeight-1 {

            let cy:Double = Double( minY) + Double(y) * zoomFactorY
            
            // for var x:Int = 0; x < bitmapWidth; ++x {
            for x:Int in 0...bitmapWidth-1 {

                let cx:Double = Double( minX) + Double( x) * zoomFactorX
                
                let valueIteration = mandelbrotPointIteration( cx: cx, cy: cy, maxSquareValue: maxSquareValue, maxIter: maxIter) * 0x09091A
                mandelbrotBitMap!.setPixel( atX: x, atY: y, rgb:UInt(valueIteration), alpha: UInt8(255))
                
                /*
                let valueIteration = mandelbrotPointIteration( cx, cy: cy, maxSquareValue: maxSquareValue, maxIter: maxIter)
                
                var r:UInt8 = 0
                var g:UInt8 = 0
                var b:UInt8 = 0
                let alpha:UInt8 = 255
                
                if valueIteration <= maxIter {
                if valueIteration<16 {
                r = UInt8( valueIteration*8 )
                g = UInt8( valueIteration*8 )
                b = UInt8( 128+valueIteration*4 )
                } else if valueIteration>=16 && valueIteration<64 {
                r = UInt8( 128+valueIteration-16 )
                g = UInt8( 128+valueIteration-16 )
                b = UInt8( 192+valueIteration-16 )
                } else if valueIteration>=64 {
                r = UInt8( maxIter-valueIteration )
                g = UInt8( 128+(maxIter-valueIteration)/2 )
                b = UInt8( maxIter-valueIteration )
                }
                
                mandelbrotBitMap!.setPixel( x, atY: y, red: r, green: g, blue: b, alpha: UInt8(255))
                
                } // else color black, i.e. alraedy preset
                */
                
                
            }
        }
        // ---------------------------------------------
        
        return mandelbrotBitMap!
    }
    
    /// -------------------------------------------------------------------------------------------------------
    /// method to calculate the Mandelbrot BitMap concurrently
    /// divides the BitMap into separate sections that will be concurrently drawn (filled)
    /// the number of sections complies with the specified numberOfConcurrentJobs
    /// The operating system controls how execution gets spread across separate threads
    func calcMandelbrotBitMap_Concurrent_Locking( numberOfConcurrentOperations:Int) -> BitMap {
        
        // ATTENTION !!! MyBitMapConcurrent the MyBitmap class with concurrent pixel setting
        mandelbrotBitMap = BitMapConcurrent( bitmapWidth, bitmapHeight)
        
        if numberOfConcurrentOperations > 0 {
            
            // ... Define JobQueue ...........................
            let operationsQueue = OperationQueue()
            
            // --- [ ------------------------------------------------------------------------------------------------------
            // ... Define the start and end index for each job ..........
            
            let sectionWidth:Int = Int(bitmapWidth / numberOfConcurrentOperations)    // the width of a section
            
            // sets the section boundaried for any than the last section
            // for var job:Int=0; job < numberOfConcurrentOperations; ++job {
            for job:Int in 0...numberOfConcurrentOperations-1 {

                self.syncLock.lock()                                                   // ... end locking
                operationsQueue.addOperation() {
                    self.calcMandelbrotBitMap_Section( startAt: job * sectionWidth, endAt: job * sectionWidth + sectionWidth)
                    /// @TODO: self.calcMandelbrotBitMap_Section( job * sectionWidth, endAt: job * sectionWidth + sectionWidth)
                    
                }
                // wait before adding next operation to ensure call parameter will not overriden by placing concurrently next operation into the queue
                self.syncLock.unlock()                                                   // ... end locking
                
            }
            
            // --- ] ------------------------------------------------------------------------------------------------------
            
            
            // ... wait until all concurrent executions in the job queue are finished
            operationsQueue.waitUntilAllOperationsAreFinished()
            
            
        } else {    // numberOfConcurrentJobs < 1
            
            // ... run single version
            // return calcMandelbrotBitMap_Single( minX, maxX:maxX, zoomFactorX: zoomFactorX, minY:minY, maxY:maxY, zoomFactorY: zoomFactorY, maxSquareValue:maxSquareValue, maxIter:maxIter)
            
            calcMandelbrotBitMap_Section( startAt: Int(0), endAt: bitmapWidth)
            
            return calcMandelbrotBitMap_Single()
        }
        
        return mandelbrotBitMap!
    }
    
    /// -------------------------------------------------------------------------------------------------------
    /// method to calculate the Mandelbrot BitMap concurrently but without locking for each pixel setting
    /// divides the BitMap into separate sections that will be concurrently drawn (filled)
    /// the number of sections complies with the specified numberOfConcurrentJobs
    /// The operating system controls how execution gets spread across separate threads
    /// ==> This version creates small gaps at the end of the section
    func calcMandelbrotBitMap_Concurrent( numberOfConcurrentOperations:Int) -> BitMap {
        
        // ATTENTION !!! not the MyBitmap class with concurrent pixel setting
        mandelbrotBitMap = BitMap( bitmapWidth, bitmapHeight)
        
        if numberOfConcurrentOperations > 0 {
            
            // ... Define JobQueue ...........................
            let operationsQueue = OperationQueue()
            
            // --- [ ------------------------------------------------------------------------------------------------------
            // ... Define the start and end index for each job ..........
            
            let sectionBitmapHeight:Int = Int( bitmapHeight / numberOfConcurrentOperations) // + 1 // the width of a section
            let sectionHeight:Double = height / Double(numberOfConcurrentOperations)              // the width of a section
            
            // sets the section boundaried for any than the last section
            // for var job:Int=0; job < numberOfConcurrentOperations - 1; ++job {
            // for var job:Int=0; job < numberOfConcurrentOperations; ++job {
            for job:Int in 0...numberOfConcurrentOperations-1 {

                self.syncLock.lock()                                                     // ... start locking
                let offsetBitmapY:Int = job * sectionBitmapHeight                            // (job == 0) ? 0 : job * (sectionBitmapWidth - 1)
                let theMinY:Double = Double(job) * sectionHeight + self.minY
                let theMaxY:Double = Double(theMinY) + Double(sectionHeight)
                let mandelbrotSection: Mandelbrot = Mandelbrot( bitmapWidth: self.bitmapWidth, bitmapHeight: sectionBitmapHeight, minX:self.minX, maxX:self.maxX, minY:theMinY, maxY:theMaxY, maxSquareValue:self.maxSquareValue, maxIter:self.maxIter)

                operationsQueue.addOperation() {
                    let theData:[UInt8] = mandelbrotSection.calcMandelbrotBitMap_Single().data
                    self.mandelbrotBitMap!.copyData( dataToCopy: theData, bitmapWidth:  self.bitmapWidth, bitmapHeight: sectionBitmapHeight, offsetBitMapY: offsetBitmapY)
                }
                // wait before adding next operation to ensure call parameter will not overriden by placing concurrently next operation into the queue
                // sleep(1)                                                                        /// @TODO: remove sleep(1) and sync access somehow
                self.syncLock.unlock()                                                   // ... end locking
                
            }
            // the last section ends at width
            
            
            // --- ] ------------------------------------------------------------------------------------------------------
            
            
            // ... wait until all concurrent executions in the job queue are finished
            operationsQueue.waitUntilAllOperationsAreFinished()
            

           

        } else {    // numberOfConcurrentJobs < 1
            
            // ... run single version
            // return calcMandelbrotBitMap_Single( minX, maxX:maxX, zoomFactorX: zoomFactorX, minY:minY, maxY:maxY, zoomFactorY: zoomFactorY, maxSquareValue:maxSquareValue, maxIter:maxIter)
            
            calcMandelbrotBitMap_Section( startAt: Int(0), endAt: bitmapWidth)
            
            return calcMandelbrotBitMap_Single()
        }
        
        return mandelbrotBitMap!
    }

    

    // *** New Version: vertical sections ***

    /// -------------------------------------------------------------------------------------------------------
    /// method to calculate the Mandelbrot BitMap concurrently but without loccking for each pixel setting
    /// divides the BitMap into separate sections that will be concurrently drawn (filled)
    /// the number of sections complies with the specified numberOfConcurrentJobs
    /// The operating system controls how execution gets spread across separate threads
    /// ==> Gapless version
    /// create sections that are bigger than the slice to calculate for the overall mandelbrot
    /// copies section (slightly bigger so that a overlap to the suceeding sections gets craeted
    /// slice is than the area that gest replaced in the target complete version
    func calcMandelbrotBitMap_Concurrent_Overlapping( numberOfConcurrentOperations:Int, numberOfOverlappingLines overlapPixels:Int = 0) -> BitMap {
                
        // ATTENTION !!! not the MyBitmap class with concurrent pixel setting
        mandelbrotBitMap = BitMap( bitmapWidth, bitmapHeight)
        
        if numberOfConcurrentOperations > 0 {
            
            // ... Define JobQueue ...........................
            let operationsQueue = OperationQueue()
            
            // --- [ ------------------------------------------------------------------------------------------------------
            // ... Define the start and end index for each job ..........
    
            let sliceBitmapHeight:Int = Int(round( Double(bitmapHeight) / Double(numberOfConcurrentOperations)))        // the height of a slice
            // let sliceBitmapHeight:Int = Int(Double(bitmapHeight) / Double(numberOfConcurrentOperations))        // the height of a slice
            let sliceHeight:Double = self.height / Double(numberOfConcurrentOperations)
            
    
            // sets the section boundaried for any than the last section
            // for var job:Int = 0; job < numberOfConcurrentOperations; ++job {
            for job:Int in 0...numberOfConcurrentOperations-1 {

                self.syncLock.lock()                                                     // ... start locking
                
                let sectionOverlap:Double = Double( Double(overlapPixels) * self.height / Double(bitmapHeight))
                let sliceBitmapOffset:Int = sliceBitmapHeight * job      // the height of a slice
                let sectionBitmapHeight:Int = sliceBitmapHeight + (job == 0 ? 1 : 2) * overlapPixels     // the width of a section
                let sectionHeight:Double = sliceHeight + (job == 0 ? 1 : 2) * sectionOverlap             // the width of a section
                
                let sectionOffsetBitmapY: Int = sliceBitmapOffset - ( job == 0 ? 0 : overlapPixels)
                let sectionMinY:Double = Double(job) * sliceHeight + self.minY - ( job == 0 ? 0 : sectionOverlap)                         // start belongs to a multiple of slice witdth to get the section start
                let sectionMaxY:Double = Double(sectionMinY) + Double(sectionHeight)
                
                let mandelbrotSection: Mandelbrot = Mandelbrot( bitmapWidth: self.bitmapWidth, bitmapHeight: sectionBitmapHeight, minX: self.minX, maxX: self.maxX, minY: sectionMinY, maxY: sectionMaxY, maxSquareValue: self.maxSquareValue, maxIter: self.maxIter)
                
                operationsQueue.addOperation() {
                    let sectionData:[UInt8] = mandelbrotSection.calcMandelbrotBitMap_Single().data
                    self.mandelbrotBitMap!.copyData( dataToCopy: sectionData, bitmapWidth: self.bitmapWidth, bitmapHeight: sectionBitmapHeight, offsetBitMapY: sectionOffsetBitmapY)
                }
                
                self.syncLock.unlock()                                                   // ... end locking

                
            }
            
            // --- ] ------------------------------------------------------------------------------------------------------
            
            
            // ... wait until all concurrent executions in the job queue are finished
            operationsQueue.waitUntilAllOperationsAreFinished()
            

        } else {    // numberOfConcurrentJobs < 1
            
            // ... run single version
            // return calcMandelbrotBitMap_Single( minX, maxX:maxX, zoomFactorX: zoomFactorX, minY:minY, maxY:maxY, zoomFactorY: zoomFactorY, maxSquareValue:maxSquareValue, maxIter:maxIter)
            
            calcMandelbrotBitMap_Section( startAt: Int(0), endAt: bitmapWidth)
            
            return calcMandelbrotBitMap_Single()
        }
        
        return mandelbrotBitMap!
    }


}
