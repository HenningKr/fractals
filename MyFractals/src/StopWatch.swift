//
//  StopWatch.swift
//  MyFractals
//
//  Created by Henning on 04.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation

class StoppWatch {
    
    private var startTime:CFAbsoluteTime? = nil
    private var endTime:CFAbsoluteTime?  = nil

    // === Calculated Properties ===================================================================
    // The calculated properties provide the stoppwatch results
    
    // --- exectime in CFAbsoluteTime, i.e. Double value.
    var execTime:Double? {
        get {
            if endTime != nil && startTime != nil {
                return endTime! - startTime!
            }
            return nil
        }
    }
    
    // --- total number of (full) seconds
    var execTime_TotalSeconds:UInt {
        if execTime != nil {
            return UInt( execTime!)
        }
        return 0
    }

    // --- total number of (full) minutes
    var execTime_TotalMinutes:UInt {
        if execTime != nil {
            return UInt( (execTime_TotalSeconds - execTime_SS) / 60)
        }
        return 0
    }

    // --- total number of (full) hours
    var execTime_TotalHours:UInt {
        if execTime != nil {
            return UInt( (execTime_TotalMinutes - execTime_MM) / 60)
        }
        return 0
    }
    
    // --- exectime in Millieseconds - complete decimal place as decimal places
    var execTime_Milliseconds:Double {
        if execTime != nil {
            return Double( execTime! - Double(execTime_TotalSeconds ))
        }
        return 0.0
    }

    // --- segragates the exec time into its parts HHH:MM:SS or DAYS-HH:MM:SS
    // ... SS - the seconds
    var execTime_SS:UInt {
        return execTime_TotalSeconds % 60
    }
    // ... MM - the Minutes
    var execTime_MM:UInt {
        return execTime_TotalMinutes % 60
        
    }
    // ... HHH - the total number hours
    var execTime_HHH:UInt {
        return execTime_TotalHours
    }
    
    // ... HH - hours 
    var execTime_HH:UInt {
        return execTime_TotalHours % 24
    }
    
    // ... DAYS - the days
    var execTime_DAYS:UInt {
        if execTime != nil {
            return UInt( (execTime_TotalHours - execTime_HH) / 24)
        }
        return 0
    }
    
    
    // ... formated time
    
    var formattedExecTime_HHH_MM_SS_MILISEC:String {
        return "\(execTime_HHH):\(execTime_MM):\(execTime_SS).\(execTime_MillisecondsUInt())"
    }

    var formattedExecTime_DAYS_HH_MM_SS_MILISEC:String {
        if execTime_DAYS > 0 {
            return "\(execTime_DAYS) Days - \(execTime_HH):\(execTime_MM):\(execTime_SS).\(execTime_MillisecondsUInt())"
            
        } else {
            return "\(execTime_HH):\(execTime_MM):\(execTime_SS).\(execTime_MillisecondsUInt())"
            
        }
    }
    
    var description:String {
        get { return formattedExecTime_DAYS_HH_MM_SS_MILISEC }
    }
    
    // === Constructors ===============================================================================
    // --- this convenience constructor would be the commonly used for initialization of the StoppWatch
    convenience init() {
        self.init( startTime: nil, endTime: nil)
    }
    // --- this constructor gets implemented to support Unit testing
    init( startTime:CFAbsoluteTime?, endTime:CFAbsoluteTime?) {
        self.startTime = startTime
        self.endTime = endTime
    }
    
    // === StopWatch functions ========================================================================
    // --- start
    func start() {
       startTime = CFAbsoluteTimeGetCurrent()
    }
    // --- stop
    func stop() {
        endTime = CFAbsoluteTimeGetCurrent()
    }
    // --- exectime in Millieseconds - complete decimal place as UInt value
    func execTime_MillisecondsUInt() -> UInt {
        if execTime != nil {
            var dblMilliseconds:Double = self.execTime_Milliseconds
            repeat {
                dblMilliseconds *= 10
            } while ( dblMilliseconds - Double( UInt(dblMilliseconds)) != 0.0 )
            
            return UInt( dblMilliseconds)
        }
        return 0
    }
    
}