//
//  MyMandelbrotSingle.swift
//  MyFractals
//
//  Created by Henning on 03.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation

/// #######################################################################################################
/// Mandelbrot

class MyMandelbrot {

    // --- Class Attributes -----------------------------------------------------------------------------------
    var minX:Double = 0.0
    var maxX:Double = 0.0
    
    var minY:Double = 0.0
    var maxY:Double = 0.0
    
    var maxSquareValue:Double = 4.0                 // 9.0
    var maxIter:Int = 32
    
    var bitmapWidth:Int = 800
    var bitmapHeight:Int = 800
    
    var mandelbrotBitMap:MyBitMap? = nil            // the MyBitMap object
    var strCommentary:String = String()

    // --- Computed Properties --------------------------------------------------------------------------------
    var width:Double { get { return self.maxX - self.minX } }
    var height:Double {  get { return self.maxY - self.minY } }
    
    var zoomFactorX:Double { get { return self.width / Double( self.bitmapWidth) } }
    var zoomFactorY:Double { get { return self.height / Double( self.bitmapHeight)} }
    
    var dataLength:Int { get { return Int( self.bitmapWidth * self.bitmapHeight * 4) } }
    
    var description:String {
        get { return "Mandelbrot set with following parameters:\n\n\t(bitmapWidth,bitmapHeight)=(\(bitmapWidth),\(bitmapHeight))\t((minX,minY),(maxX,maxY))=((\(minX), \(minY)),(\(maxX), \(maxY)))\n\tZoom Factor X: \(zoomFactorX)\tZoom Factor Y: \(zoomFactorY)\n\tmaximum square vale: \(maxSquareValue)\n\tmaximum iterations:\(maxIter)\n\(strCommentary)" }
    }

     /// --- Constructor ---------------------------------------------------------------------------------------
    
    init( bitmapWidth:Int = 800, bitmapHeight:Int = 800, minX:Double = -2.0, maxX:Double = 0.5, minY:Double = -1.25, maxY:Double = 1.25, maxSquareValue:Double = 4.0, maxIter:Int = 32) {
        
        self.bitmapWidth = bitmapWidth
        self.bitmapHeight = bitmapHeight
        
        self.minX = minX
        self.maxX = maxX
        
        self.minY = minY
        self.maxY = maxY
        
        self.maxSquareValue = maxSquareValue
        self.maxIter = maxIter
        
    }
    
    
    /// -------------------------------------------------------------------------------------------------------
    /// function to calculate mandelbrot value for one oixel
    func mandelbrotPointIteration( cx: Double, cy: Double, maxSquareValue:Double, maxIter: Int) -> Int {
        
        var iter:Int = 0
        var x:Double = 0.0
        var y:Double = 0.0

        var xSquare:Double = 0.0
        var ySquare:Double = 0.0
        
        while  ( (xSquare + ySquare) <= maxSquareValue ) && ( iter <= maxIter ) {
            let xTemp:Double = xSquare - (ySquare + cx)
            let yTemp:Double = 2 * x * y + cy
            x = xTemp
            y = yTemp
            xSquare = x*x
            ySquare = y*y
            iter += 1
        }
        
        return iter
        
        // return( iter – log(log(betrag_quadrat) / log(4)) / log(2)) ... would return Double
        // alternative for a continous colour flow:
        // punkt_iteration = iter – log(log(betrag_quadrat) / log(4)) / log(2)
    }

    
    /// -------------------------------------------------------------------------------------------------------
    ///
    func calcMandelbrotBitMap_Section( startAt: Int, endAt:Int) {
        
        // --- custom operation work is done here. ----
        // ... for the boundaries of the section do the Mandelbrot algorithm
        // for var x:UInt = 0; x < bitmapWidth; x++ {
        // for var y:Int = 0; y < bitmapHeight; ++y {
        for y:Int in 0...bitmapHeight {

            let cy:Double = Double( minY) + Double(y) * zoomFactorY
            
            // for var x:Int = startAt; x <= endAt; ++x {
            for x:Int in startAt...endAt {

                let cx:Double = Double( minX) + Double( x) * zoomFactorX
            
                let valueIteration = mandelbrotPointIteration( cx: cx, cy: cy, maxSquareValue: maxSquareValue, maxIter: maxIter) * 0x09091A
                mandelbrotBitMap!.setPixel( atX: x, atY: y, rgb:UInt(valueIteration), alpha: UInt8(255))

                /*
                let valueIteration = mandelbrotPointIteration( cx, cy: cy, maxSquareValue: maxSquareValue, maxIter: maxIter)
                
                var r:UInt8 = 0
                var g:UInt8 = 0
                var b:UInt8 = 0
                let alpha:UInt8 = 255
                
                if valueIteration <= maxIter {
                    if valueIteration<16 {
                        r = UInt8( valueIteration*8 )
                        g = UInt8( valueIteration*8 )
                        b = UInt8( 128+valueIteration*4 )
                    } else if valueIteration>=16 && valueIteration<64 {
                        r = UInt8( 128+valueIteration-16 )
                        g = UInt8( 128+valueIteration-16 )
                        b = UInt8( 192+valueIteration-16 )
                    } else if valueIteration>=64 {
                        r = UInt8( maxIter-valueIteration )
                        g = UInt8( 128+(maxIter-valueIteration)/2 )
                        b = UInt8( maxIter-valueIteration )
                    }
                    
                    mandelbrotBitMap!.setPixel( x, atY: y, red: r, green: g, blue: b, alpha: UInt8(255))

                } // else color black, i.e. alraedy preset
                */
                
                
            }
        }
        // ---------------------------------------------
    }

    /// -------------------------------------------------------------------------------------------------------
    /// method to calculate the Mandelbrot BitMap without any concurrency
    func calcMandelbrotBitMap_Single() -> MyBitMap {

        // the bitmap initialization
        // ATTENTION !!! not the MyBitmap class with concurrent pixel setting
        mandelbrotBitMap = MyBitMap( bitmapWidth, bitmapHeight)
        
        calcMandelbrotBitMap_Section( startAt: Int(0), endAt: bitmapWidth - 1)
        
        return mandelbrotBitMap!
    }
    
    /// -------------------------------------------------------------------------------------------------------
    /// method to calculate the Mandelbrot BitMap concurrently
    /// divides the BitMap into separate sections that will be concurrently drawn (filled)
    /// the number of sections complies with the specified numberOfConcurrentJobs
    /// The operating system controls how execution gets spread across separate threads
    func calcMandelbrotBitMap_Concurrent( numberOfConcurrentOperations:Int) -> MyBitMap {
        
        // ATTENTION !!! MyBitMapConcurrent the MyBitmap class with concurrent pixel setting
        mandelbrotBitMap = MyBitMapConcurrent( bitmapWidth, bitmapHeight)
        
        if numberOfConcurrentOperations > 0 {
            
            // ... Define JobQueue ...........................
            let operationsQueue = OperationQueue()
            
            // --- [ ------------------------------------------------------------------------------------------------------
            // ... Define the start and end index for each job ..........
            
            let sectionWidth:Int = Int(bitmapWidth / numberOfConcurrentOperations)    // the width of a section

            // sets the section boundaried for any than the last section
            // for var job:Int=0; job < numberOfConcurrentOperations - 1; ++job {
            for job:Int in 0...numberOfConcurrentOperations - 1 {

                operationsQueue.addOperation() {
                    self.calcMandelbrotBitMap_Section( startAt: job * sectionWidth, endAt: job * sectionWidth + sectionWidth - 1)
                }
                // wait before adding next operation to ensure call parameter will not overriden by placing concurrently next operation into the queue
                sleep(1)
                
            }
            // the last section ends at width

            operationsQueue.addOperation() {
                self.calcMandelbrotBitMap_Section( startAt: ( numberOfConcurrentOperations - 1)*sectionWidth, endAt:  self.bitmapWidth)
            }
            // --- ] ------------------------------------------------------------------------------------------------------
            
        
            // ... wait until all concurrent executions in the job queue are finished
            operationsQueue.waitUntilAllOperationsAreFinished()

 
        } else {    // numberOfConcurrentJobs < 1
            
            // ... run single version
            // return calcMandelbrotBitMap_Single( minX, maxX:maxX, zoomFactorX: zoomFactorX, minY:minY, maxY:maxY, zoomFactorY: zoomFactorY, maxSquareValue:maxSquareValue, maxIter:maxIter)
            
            calcMandelbrotBitMap_Section( startAt: Int(0), endAt: bitmapWidth)

            return calcMandelbrotBitMap_Single()
        }
        
      return mandelbrotBitMap!
   }

    /// -------------------------------------------------------------------------------------------------------
    /// method to calculate the Mandelbrot BitMap concurrently but without loccking for each pixel setting
    /// divides the BitMap into separate sections that will be concurrently drawn (filled)
    /// the number of sections complies with the specified numberOfConcurrentJobs
    /// The operating system controls how execution gets spread across separate threads
    func calcMandelbrotBitMap_ConcurrentLockfreeConcurrentLockfree_XXX( numberOfConcurrentOperations:Int) -> MyBitMap {
      
        // ATTENTION !!! not the MyBitmap class with concurrent pixel setting
        mandelbrotBitMap = MyBitMap( bitmapWidth, bitmapHeight)
        
        if numberOfConcurrentOperations > 0 {
            
            // ... Define JobQueue ...........................
            let operationsQueue = OperationQueue()
            
            // --- [ ------------------------------------------------------------------------------------------------------
            // ... Define the start and end index for each job ..........
            
            let sectionBitmapWidth:Int = Int( bitmapWidth / numberOfConcurrentOperations)  // the width of a section
            let sectionWidth:Double = width / Double(numberOfConcurrentOperations)         // the width of a section
            
            // sets the section boundaried for any than the last section
            // for var job:Int=0; job < numberOfConcurrentOperations - 1; ++job {
            for job:Int in 0...numberOfConcurrentOperations - 1 {

                operationsQueue.addOperation() {
                    
                    let offsetBitmapX:Int = job * sectionBitmapWidth
                    let theMinX:Double = Double(job) * sectionWidth + self.minX
                    let theMaxX:Double = Double(theMinX) + Double(sectionWidth)
                    
                    let myMandelbrotSection: MyMandelbrot = MyMandelbrot( bitmapWidth: sectionBitmapWidth+1, bitmapHeight: self.bitmapHeight, minX:theMinX, maxX:theMaxX, minY:self.minY, maxY:self.maxY, maxSquareValue:self.maxSquareValue, maxIter:self.maxIter)
                    
                    let theData:[UInt8] = myMandelbrotSection.calcMandelbrotBitMap_Single().data
                    self.mandelbrotBitMap!.copyData( offsetBitMapX: offsetBitmapX, dataToCopy: theData, bitmapWidth: sectionBitmapWidth+1, bitmapHeight: self.bitmapHeight )
                    
                }
                // wait before adding next operation to ensure call parameter will not overriden by placing concurrently next operation into the queue
                sleep(1)
                
            }
            // the last section ends at width
            
            operationsQueue.addOperation() {
                // self.calcMandelbrotBitMap_Section( ( numberOfConcurrentOperations - 1)*sectionWidth, endAt:  self.bitmapWidth - 1)
                
                let offsetBitmap:Int = ( numberOfConcurrentOperations - 1 ) * sectionBitmapWidth
                let theMinX:Double = Double( numberOfConcurrentOperations - 1) * sectionWidth + self.minX
                let theMaxX:Double = Double(theMinX) + Double(sectionWidth)
                
                let myMandelbrotSection: MyMandelbrot = MyMandelbrot( bitmapWidth: sectionBitmapWidth, bitmapHeight: self.bitmapHeight, minX:theMinX, maxX:theMaxX, minY:self.minY, maxY:self.maxY, maxSquareValue:self.maxSquareValue, maxIter:self.maxIter)
                
                let theData:[UInt8] = myMandelbrotSection.calcMandelbrotBitMap_Single().data
                self.mandelbrotBitMap!.copyData( offsetBitMapX: offsetBitmap, dataToCopy: theData, bitmapWidth: sectionBitmapWidth, bitmapHeight: self.bitmapHeight )

            }
            // --- ] ------------------------------------------------------------------------------------------------------
            
            
            // ... wait until all concurrent executions in the job queue are finished
            operationsQueue.waitUntilAllOperationsAreFinished()
            
            
        } else {    // numberOfConcurrentJobs < 1
            
            // ... run single version
            // return calcMandelbrotBitMap_Single( minX, maxX:maxX, zoomFactorX: zoomFactorX, minY:minY, maxY:maxY, zoomFactorY: zoomFactorY, maxSquareValue:maxSquareValue, maxIter:maxIter)
            
            calcMandelbrotBitMap_Section( startAt: Int(0), endAt: bitmapWidth)
            
            return calcMandelbrotBitMap_Single()
        }
        
        return mandelbrotBitMap!
    }

    
    
}

