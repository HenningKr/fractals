//
//  TmpFile1.swift
//  MyFractals
//
//  Created by Henning on 31.03.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//
/*
import Foundation

// Playground - noun: a place where people can play

import Cocoa

class MyView : NSView {
    override func drawRect( rect: NSRect) {
        NSColor.greenColor().setFill()
        let path = NSBezierPath( rect: self.bounds)
        path.fill()
    }
}

let viewRect = NSRect( x:0, y:0, width:100, height:100)
let myView = MyView( frame: viewRect)

/* =====================================================================================
import java.awt.*;
import java.awt.event.*;

public class MandelbrotFrame extends Frame
{
// drawing the mandelbrot set
// by kopfsalat

private int width, height;
private double left, top, right, bottom;
private double xside, yside, xscale, yscale;

public MandelbrotFrame (int width, int height, double left, double top, double right, double bottom)
{
// creating the frame
super("Mandelbrot");

// setting some variables for calculation
this.width  = width;
this.height = height;
this.left   = left;
this.right  = right;
this.top    = top;
this.bottom = bottom;

this.xside  = right - left;
this.yside  = bottom - top;
this.xscale = xside / width;
this.yscale = yside / height;

// set some parameters and the listener for the frame
this.setSize(width, height);
this.setVisible(true);
addWindowListener(new WindowAdapter() {   public void windowClosing(WindowEvent e) { System.exit(0); } } );
}

public void paint(Graphics g)
{
double cx, cy, zx, zy, zxPow, zyPow, tempx;
int colorcounter, maxiterations = 32;

for (int y = 0; y < height; y++)
{
for (int x = 0; x < width; x++)
{
cx = x * xscale + left;
cy = y * yscale + top;
zx = 0; zxPow = 0;
zy = 0; zyPow = 0;
colorcounter = 0;

while (zxPow + zyPow < 4.0 && colorcounter < maxiterations)
{
tempx = zxPow - zyPow + cx;
zy = 2.0 * zx * zy + cy;
zx = tempx;
colorcounter++;
zxPow = zx * zx;
zyPow = zy * zy;
}
g.setColor(new Color(colorcounter * 0x09091A)); // play with this value for other coloreffects
g.drawRect(x,y,0,0);
}
}
}

public static void main (String [] args)
{
new MandelbrotFrame (800, 800, -2.0, 1.25, 0.5, -1.25);
// new MandelbrotFrame (750, 750, -2.0, 1.25, 0.5, -1.25);
}
}


------------------------------------------------------------------------
graphsize 384,384
refresh
kt=319 : m = 4.0
xmin=2.1 : xmax=-0.6 : ymin=-1.35 : ymax=1.35
dx=(xmax-xmin)/graphwidth : dy=(ymax-ymin)/graphheight

for x=0 to graphwidth
jx = xmin+x*dx
for y=0 to graphheight
jy = ymin+y*dy
k = 0 : wx = 0.0 : wy = 0.0
do
tx = wx*wx-(wy*wy+jx)
ty = 2.0*wx*wy+jy
wx = tx
wy = ty
r = wx*wx+wy*wy
k = k+1
until r>m or k>kt

if k>kt then
color black
else
if k<16 then color k*8,k*8,128+k*4
if k>=16 and k<64 then color 128+k-16,128+k-16,192+k-16
if k>=64 then color kt-k,128+(kt-k)/2,kt-k
end if
plot x,y
next y
refresh
next x
imgsave "Mandelbrot_BASIC-256.png", "PNG"

======================================================================================== */

// ================== way to draw a bitmap and show that ============================================================

// see alos https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CocoaDrawingGuide/Images/Images.html

import CoreGraphics

func doMyBitMap( width: UInt, height: UInt) -> NSImage {     // --> CGImageRef {

    //Create a raw buffer to hold pixel data which we will fill algorithmically
    // ... let width:UInt = UInt(100)
    // ... let height:UInt = UInt(100)
    
    let dataLength:Int = Int(width * height * 4)
    
    var data:[UInt8] = [UInt8]()   // dataLength
    data.reserveCapacity( Int(dataLength))
    
    //Fill pixel buffer with color data
    for (var j:Int=0; j < Int(height); j++) {
        for (var i:Int=0; i < Int(width); i++) {
            
            //Here I'm just filling every pixel with red
            let red   = 1.0
            let green = 0.75
            let blue  = 0.25
            let alpha = 1.0
            
            var index:Int = 4 * ( i + j * Int(width))
            
            // println("j = \(j); i = \(i); index = \(index)")
            
            data.insert( UInt8(255*red), atIndex: index)
            data.insert( UInt8(255*green), atIndex: index+1)
            data.insert( UInt8(255*blue), atIndex: index+2)
            data.insert( UInt8(255*alpha), atIndex: index+3)
            
        }
    }
    
    // Create a CGImage with the pixel data
    let provider:CGDataProviderRef  = CGDataProviderCreateWithData( nil, data, UInt(dataLength), nil)
    let colorspace:CGColorSpaceRef  = CGColorSpaceCreateDeviceRGB()
    
    let bitsPerComponent:UInt = UInt(8)
    let bitsPerPixel:UInt = UInt(32)
    let bytesPerRow:UInt = UInt(width * 4)
    let bitmapInfo:CGBitmapInfo = CGBitmapInfo( UInt32(CGBitmapInfo.ByteOrder32Big.rawValue|CGImageAlphaInfo.PremultipliedLast.rawValue))
    
    let image:CGImageRef = CGImageCreate( width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorspace, bitmapInfo, provider, nil, true, kCGRenderingIntentDefault)
    
    var size:NSSize = NSSize()
    size.width = CGFloat( width)
    size.height = CGFloat( height)
    
    return NSImage( CGImage: image, size: size)
}


class MyBitmapView : NSImageView {
    
    override init(frame frameRect: NSRect) {
        super.init( frame: frameRect)
    }
    
    required init?(coder: NSCoder) {
        super.init( coder: coder)
    }
    
    override func drawRect( rect: NSRect) {
        super.drawRect( rect)
        let image:NSImage = doMyBitMap( UInt(20), UInt(20))
        image.drawInRect(rect)
        // println( "Image size: \(image.size)")        
        self.image = image
    }
}


var startTime = CFAbsoluteTimeGetCurrent()

let viewBitmapRect = NSRect( x:0, y:0, width:100, height:100)
let myBitmapView = MyBitmapView( frame: viewBitmapRect)

println(" R RENDER: \(CFAbsoluteTimeGetCurrent() - startTime)")


// see also http://blog.human-friendly.com/drawing-images-from-pixel-data-in-swift
// see also http://www.raywenderlich.com/76285/beginning-core-image-swift


/// #######################################################################################################
/// Mandelbrot
/// -------------------------------------------------------------------------------------------------------
/// function to calculate mandelbrot value for one oixel
func mandelbrot_punkt_iteration( cx: Double, cy: Double, max_betrag_quadrat:Double, max_iter: UInt) -> UInt {
    
    var betrag_quadrat:Double = 0
    var iter:UInt = 0
    var x:Double = 0
    var y:Double = 0
    
    while  ( betrag_quadrat <= max_betrag_quadrat ) && ( iter < max_iter ) {
        let xt:Double = x * x - y * y + cx
        let yt:Double = 2 * x * y + cy
        x = xt
        y = yt
        iter++
        betrag_quadrat = x * x + y * y
    }
    
    return iter
    // return( iter – log(log(betrag_quadrat) / log(4)) / log(2)) ... would return Double
    // alternative for a continous colour flow:
    // punkt_iteration = iter – log(log(betrag_quadrat) / log(4)) / log(2)
    
}

/// -------------------------------------------------------------------------------------------------------
/// function to build the image
func mandelbrot_image( min_x:UInt, max_x:UInt, zoom_factor_x: Double, min_y:UInt, max_y:UInt, zoom_factor_y: Double, max_betrags_quadrat:Double = 570.0, max_iterations:UInt = 100) -> NSImage {
    
    /*
    // Getting the main queue (will run on teh main thread)
    var mainQueue = NSOperationQueue.mainQueue()
    // Creating a new queue (will ron on a background thread, probably)
    */
    
    
    
    /* * /
    var backgroundQueue = NSOperationQueue()
    / * */
    
    
    // ... BEGIN: prepare image data .................
    let width = max_x - min_x
    let height = max_y - min_y
    let dataLength:Int = Int(width * height * 4)
    var data:[UInt8] = [UInt8]()   // dataLength
    data.reserveCapacity( Int(dataLength))
    // ... END: prepare image data ...................
    
    for var x:UInt = UInt(0); x < max_x; x++ {
        
        // Operation queue for paraleleisation
        
        let cx:Double = Double(min_x) + Double( x) * zoom_factor_x
        
        for var y:UInt = 0; y < max_y; y++ {
            
            
            /*
            
            backgroundQueue.maxConcurrentOperationCount
            backgroundQueue.operationCount
            
            // Wait for adding further operation blocks
            if backgroundQueue.operationCount >= 3  {
            // sleep( 1000)
            backgroundQueue.waitUntilAllOperationsAreFinished()
            }
            */
            
            let pix_x = x
            let pix_y = y
            
            let myCx = cx
            
            /* * /
            backgroundQueue.addOperationWithBlock() {
            / * */
            let myCy:Double = Double(min_y) + Double(pix_y) * zoom_factor_y
            
            let iterations_wert = mandelbrot_punkt_iteration( myCx, myCy, max_betrags_quadrat, max_iterations)
            
            let (r:UInt8,g:UInt8,b:UInt8) = get_color( iterations_wert)
            
            // ... BEGIN: set pixel value ...........................
            // --- var index:Int = Int(4 * (Int(cx) + Int(cy) * Int(width)))
            var index:Int = Int(4 * (Int(pix_x) + Int(pix_y)))
            let alpha:UInt8 = UInt8(255)
            data.insert( r, atIndex: index)
            data.insert( g, atIndex: index+1)
            data.insert( b, atIndex: index+2)
            data.insert( alpha, atIndex: index+3)
            // ... END: set pixel ....................................
            // ... [End the code block in the closure]
            
            /*
            // put any final work onto the GUI, this can be doen only on the main queue
            mainQueue.addOperationWithBlock() {
            // GUI
            
            }
            */
            /* * /
            }
            / * */
            
        }
        
    }
    
    /* * /
    // Sync with background queue
    backgroundQueue.waitUntilAllOperationsAreFinished()
    / * */
    
    // ... BEGIN: create the image ...........................................................................................
    // Create a CGImage with the pixel data
    let provider:CGDataProviderRef  = CGDataProviderCreateWithData( nil, data, UInt(dataLength), nil)
    let colorspace:CGColorSpaceRef  = CGColorSpaceCreateDeviceRGB()
    let bitsPerComponent:UInt = UInt(8)
    let bitsPerPixel:UInt = UInt(32)
    let bytesPerRow:UInt = UInt(width * 4)
    let bitmapInfo:CGBitmapInfo = CGBitmapInfo( UInt32(CGBitmapInfo.ByteOrder32Big.rawValue|CGImageAlphaInfo.PremultipliedLast.rawValue))
    let image:CGImageRef = CGImageCreate( width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorspace, bitmapInfo, provider, nil, true, kCGRenderingIntentDefault)
    var size:NSSize = NSSize()
    size.width = CGFloat( width)
    size.height = CGFloat( height)
    return NSImage( CGImage: image, size: size)
    // ... END:create the image ...........................................................................................
    
}

/// -------------------------------------------------------------------------------------------------------
// function to transform value into r,g,b
func get_color( uintValue:UInt) -> (r:UInt8, g:UInt8, b:UInt8) {
    
    // let rgbUintValue = uintValue % (255 * 255 * 255)
    
    let red:UInt8 = UInt8( (uintValue & 255) % 255)
    let green:UInt8 = UInt8( UInt8( ((uintValue >> 8) & 255)) % 255 )
    let blue:UInt8 = UInt8( UInt8(  ((uintValue >> 16) & 255))  % 255 )
    
    return ( red, green, blue)
    
}

/// -------------------------------------------------------------------------------------------------------
///

class MyMandelbrotView : NSImageView {
    
    var min_x:Double = 0.0
    var max_x:Double = 800.0
    var zoom_x:Double = 150.0
    
    var min_y:Double = 0.0
    var max_y:Double = 600.0
    var zoom_y:Double = 150.0
    
    var max_betrags_quadrat:Double = 570.0
    var max_iterations:UInt = 150
    
    
    convenience init( frame frameRect: NSRect, _ min_x:Double, _ max_x:Double, _ zoom_x:Double, _ min_y:Double, _ max_y:Double, _ zoom_y: Double, _ max_betrags_quadrat:Double, _ max_iterations: UInt) {
        
        self.init( frame: frameRect)
        
        self.min_x = min_x
        self.max_x = max_x
        self.zoom_x = zoom_x
        
        self.min_y = min_y
        self.max_y = max_y
        self.zoom_y = zoom_y
        
        self.max_betrags_quadrat = max_betrags_quadrat
        self.max_iterations = max_iterations
    }
    
    override init(frame frameRect: NSRect) {
        super.init( frame: frameRect)
    }
    
    required init?(coder: NSCoder) {
        super.init( coder: coder)
    }
    
    override func drawRect( rect: NSRect) {
        super.drawRect( rect)
        let image:NSImage = mandelbrot_image( UInt(min_x), UInt(max_x), Double(zoom_x), UInt(min_y), UInt(max_y), Double(zoom_y), max_betrags_quadrat: max_betrags_quadrat, max_iterations: max_iterations)
        image.drawInRect(rect)
        // println( "Image size: \(image.size)")
        
        self.image = image
    }
}



/// -------------------------------------------------------------------------------------------------------
let max_betrags_quadrat:Double = 4 // 570.0       // test with bigger values: 255 * 255 * 255
let max_iterations:UInt = 100 // UInt(150)              // bigger value in zoomes zones necessary
let zoom:Double = 150.0

startTime = CFAbsoluteTimeGetCurrent()

/*
let viewMandelbrotBitmapRect = NSRect( x:0, y:0, width:800, height:600)
let myMandelbrotView = MyMandelbrotView( frame: viewMandelbrotBitmapRect, 100.0, 800.0, zoom, 100.0, 600.0, zoom, max_betrags_quadrat, max_iterations)
*/

let viewMandelbrotBitmapRect = NSRect( x:0, y:0, width:400, height:300)
let myMandelbrotView = MyMandelbrotView( frame: viewMandelbrotBitmapRect, 100.0, 500.0, zoom, 100.0, 400.0, zoom, max_betrags_quadrat, max_iterations)

println(" R RENDER: \(CFAbsoluteTimeGetCurrent() - startTime)")


*/
/*

--- Iteration über alle Pixel:

max_iteration = 1000        // at minimum 100

FOR pix_x = 1 TO max_x
cx = min_cx + pix_x * punkt_abstand_x                   // punkt_abstand_x -- Zoomfaktor x-Dimenion

FOR pix_y = 1 TO max_y
cy = min_cy + pix_y * punkt_abstand_y               // punkt_abstand_y - Zoomfaktor y Dimension

iterations_wert = punkt_iteration ( cx, cy, max_betrags_quadrat, max_iterationen )

farb_wert = waehle_farbe ( iterations_wert, max_iterationen )

plot pix_x pix_y farb_wert

NEXT pix_y
NEXT pix_x

--- Iteration eines Bildpunktes
FUNCTION punkt_iteration (cx, cy, max_betrag_quadrat, max_iter)

betrag_quadrat = 0
iter = 0
x = 0
y = 0

WHILE ( betrag_quadrat <= max_betrag_quadrat ) AND ( iter < max_iter )
xt = x * x - y * y + cx
yt = 2 * x * y + cy
x = xt
y = yt
iter = iter + 1
betrag_quadrat = x * x + y * y
END

punkt_iteration = iter
// alternative for a continous colour flow:
// punkt_iteration = iter – log(log(betrag_quadrat) / log(4)) / log(2)

END FUNCTION




*/

/* --- Java ---
import java.awt.*;
import java.applet.*;

public class Apfelmaennchen extends Applet {
public void init() {
setBackground(new Color(255,255,255)); //Hintergrundfarbe Applet
}

//C-Werte checken nach Zn+1 = Zn^2 + C, Zo = 0. 30 Iterationen.
public int checkC(double reC,double imC) {
double reZ=0,imZ=0,reZ_minus1=0,imZ_minus1=0;
int 0;
for (i=0;i<30;i++) {
imZ=2*reZ_minus1*imZ_minus1+imC;
reZ=reZ_minus1*reZ_minus1-imZ_minus1*imZ_minus1+reC;
if (reZ*reZ+imZ*imZ>4) return i;
reZ_minus1=reZ;
imZ_minus1=imZ;
}
return i;
}

//Punkte berechnen und setzen.
public void paint (Graphics g) {
double reC, imC, zelle=0.00625; //Ein Pixel = 0.00625
int x,y;
Color colAppleman = new Color(0,129,190); //Farbe Apfelmännchen

imC=-1.1; //oberer Rand
for (y=0;y<350;y++) {
reC=-2.1; //linker Rand
for (x=0;x<440;x++) {
if(checkC(reC,imC)==30) {
g.setColor(colAppleman);
g.drawLine(x,y,x,y);
}
reC=reC+zelle; //nächste Spalte
}
imC=imC+zelle; //nächste Zeile
}
}
}
*/

/* --- C++
#include <cstdlib>
#include <complex>

// get dimensions for arrays
template<typename ElementType, std::size_t dim1, std::size_t dim2>
std::size_t get_first_dimension(ElementType (&a)[dim1][dim2])
{
return dim1;
}

template<typename ElementType, std::size_t dim1, std::size_t dim2>
std::size_t get_second_dimension(ElementType (&a)[dim1][dim2])
{
return dim2;
}


template<typename ColorType, typename ImageType>
void draw_Mandelbrot(ImageType& image,                                   //where to draw the image
ColorType set_color, ColorType non_set_color,       //which colors to use for set/non-set points
double cxmin, double cxmax, double cymin, double cymax,//the rect to draw in the complex plane
unsigned int max_iterations)                          //the maximum number of iterations
{
std::size_t const ixsize = get_first_dimension(ImageType);
std::size_t const iysize = get_first_dimension(ImageType);
for (std::size_t ix = 0; ix < ixsize; ++ix)
for (std::size_t iy = 0; iy < iysize; ++iy)
{
std::complex<double> c(cxmin + ix/(ixsize-1.0)*(cxmax-cxmin), cymin + iy/(iysize-1.0)*(cymax-cymin));
std::complex<double> z = 0;
unsigned int iterations;

for (iterations = 0; iterations < max_iterations && std::abs(z) < 2.0; ++iterations)
z = z*z + c;

image[ix][iy] = (iterations == max_iterations) ? set_color : non_set_color;

}
}
*/


/* --- C#

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Forms;

/// <summary>
/// Generates bitmap of Mandelbrot Set and display it on the form.
/// </summary>
public class MandelbrotSetForm : Form
{
const double MaxValueExtent = 2.0;
Thread thread;

static double CalcMandelbrotSetColor(ComplexNumber c)
{
// from http://en.wikipedia.org/w/index.php?title=Mandelbrot_set
const int MaxIterations = 1000;
const double MaxNorm = MaxValueExtent * MaxValueExtent;

int iteration = 0;
ComplexNumber z = new ComplexNumber();
do
{
z = z * z + c;
iteration++;
} while (z.Norm() < MaxNorm && iteration < MaxIterations);
if (iteration < MaxIterations)
return (double)iteration / MaxIterations;
else
return 0; // black
}

static void GenerateBitmap(Bitmap bitmap)
{
double scale = 2 * MaxValueExtent / Math.Min(bitmap.Width, bitmap.Height);
for (int i = 0; i < bitmap.Height; i++)
{
double y = (bitmap.Height / 2 - i) * scale;
for (int j = 0; j < bitmap.Width; j++)
{
double x = (j - bitmap.Width / 2) * scale;
double color = CalcMandelbrotSetColor(new ComplexNumber(x, y));
bitmap.SetPixel(j, i, GetColor(color));
}
}
}

static Color GetColor(double value)
{
const double MaxColor = 256;
const double ContrastValue = 0.2;
return Color.FromArgb(0, 0,
(int)(MaxColor * Math.Pow(value, ContrastValue)));
}

public MandelbrotSetForm()
{
// form creation
this.Text = "Mandelbrot Set Drawing";
this.BackColor = System.Drawing.Color.Black;
this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
this.MaximizeBox = false;
this.StartPosition = FormStartPosition.CenterScreen;
this.FormBorderStyle = FormBorderStyle.FixedDialog;
this.ClientSize = new Size(640, 640);
this.Load += new System.EventHandler(this.MainForm_Load);
}

void MainForm_Load(object sender, EventArgs e)
{
thread = new Thread(thread_Proc);
thread.IsBackground = true;
thread.Start(this.ClientSize);
}

void thread_Proc(object args)
{
// start from small image to provide instant display for user
Size size = (Size)args;
int width = 16;
while (width * 2 < size.Width)
{
int height = width * size.Height / size.Width;
Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
GenerateBitmap(bitmap);
this.BeginInvoke(new SetNewBitmapDelegate(SetNewBitmap), bitmap);
width *= 2;
Thread.Sleep(200);
}
// then generate final image
Bitmap finalBitmap = new Bitmap(size.Width, size.Height, PixelFormat.Format24bppRgb);
GenerateBitmap(finalBitmap);
this.BeginInvoke(new SetNewBitmapDelegate(SetNewBitmap), finalBitmap);
}

void SetNewBitmap(Bitmap image)
{
if (this.BackgroundImage != null)
this.BackgroundImage.Dispose();
this.BackgroundImage = image;
}

delegate void SetNewBitmapDelegate(Bitmap image);

static void Main()
{
Application.Run(new MandelbrotSetForm());
}
}

struct ComplexNumber
{
public double Re;
public double Im;

public ComplexNumber(double re, double im)
{
this.Re = re;
this.Im = im;
}

public static ComplexNumber operator +(ComplexNumber x, ComplexNumber y)
{
return new ComplexNumber(x.Re + y.Re, x.Im + y.Im);
}

public static ComplexNumber operator *(ComplexNumber x, ComplexNumber y)
{
return new ComplexNumber(x.Re * y.Re - x.Im * y.Im,
x.Re * y.Im + x.Im * y.Re);
}

public double Norm()
{
return Re * Re + Im * Im;
}
}

*/

/* --- Java
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class Mandelbrot extends JFrame {

private final int MAX_ITER = 570;
private final double ZOOM = 150;
private BufferedImage I;
private double zx, zy, cX, cY, tmp;

public Mandelbrot() {
super("Mandelbrot Set");
setBounds(100, 100, 800, 600);
setResizable(false);
setDefaultCloseOperation(EXIT_ON_CLOSE);
I = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
for (int y = 0; y < getHeight(); y++) {
for (int x = 0; x < getWidth(); x++) {
zx = zy = 0;
cX = (x - 400) / ZOOM;
cY = (y - 300) / ZOOM;
int iter = MAX_ITER;
while (zx * zx + zy * zy < 4 && iter > 0) {
tmp = zx * zx - zy * zy + cX;
zy = 2.0 * zx * zy + cY;
zx = tmp;
iter--;
}
I.setRGB(x, y, iter | (iter << 8));
}
}
}

@Override
public void paint(Graphics g) {
g.drawImage(I, 0, 0, this);
}

public static void main(String[] args) {
new Mandelbrot().setVisible(true);
}
}
*/



