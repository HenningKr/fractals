//
//  main.swift
//  FractalsCmdLine_1
//
//  Created by Henning on 17.04.15.
//  Copyright (c) 2015 Hans-Henning Kruse. All rights reserved.
//

import Foundation

// --- (1) specify direcory and Filename for saving -------------------------------------------------------------
// --- (1.1) users home directory to save the BitMap as TIFF
let dirs : [String]? = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String]
// if there are subfolders in users home
if (dirs != nil) {
    // use the documents folder
    let directories:[String] = dirs!
    let dir = directories[0]            // users 'documents' directory
    
    
    // --- (2) Create the Mandelbrot image ------------------------------------------------------------------------
    // ... Creating the Mandelbrot Bitmap .............................................................
    
    let bitmapWidth:Int = 1024  // 800      // 384      //  800 // 800 = 4*200
    let bitmapHeight:Int = 1024     // 800     // 384     // 500 //    = 2.5 * 200              800
    
    let minX:Double = -1.0      // -2.0          // 2.1           // -2.0
    let maxX:Double = 2.5       // 0.5           // -0.6          // 2.0 // 0.5
    let minY:Double = -1.5      // -1.25          // -1.35         // -1.25
    let maxY:Double = 1.5 // 1.25          // 1.35          // 1.25
    
    let maxSquareValue:Double = 144.0       // 1000.0            // 144.0    // 4.0
    let maxIter:Int = 1000                  // 100000                      // 1000             // 32

    // --- (3) Test MyMandelbrot (not concurrent) ---------------------------------------------------------------------------------------------------------------------------------------------
    // let myMandelbrot:MyMandelbrot = MyMandelbrot()
    // let mandelbrotBitMap:MyBitMap = myMandelbrot.calcMandelbrotBitMap_Single( minX, maxX:maxX, zoomFactorX: zoomX, minY:minY, maxY:maxY, zoomFactorY:zoomY, maxSquareValue:maxSquareValue, maxIter:maxIter)
    
    // --- (3.1) Specify the FileName
    var fileName = "myMandelbrotTest_Single_1.tif"  // the filename to save
    
    let stoppWatch:StoppWatch = StoppWatch()
    stoppWatch.start()
    let myMandelbrot:MyMandelbrot = MyMandelbrot( bitmapWidth: bitmapWidth, bitmapHeight: bitmapHeight, minX:minX, maxX:maxX, minY:minY, maxY:maxY, maxSquareValue:maxSquareValue, maxIter:maxIter)
    let myMandelbrotBitMap:MyBitMap = myMandelbrot.calcMandelbrotBitMap_Single()
    stoppWatch.stop()
    
    // ... Saving the Bitmap .............................................................
    myMandelbrotBitMap.saveBitMapAsTIFF( dir, toFileName: fileName )
    
    print("Finished creating and writing Mandelbrot Image to file \(fileName) in path: \(dir) to users home.\n\(myMandelbrot)\nTime need for execution: \(stoppWatch.execTime) => \(stoppWatch.formattedExecTime_DAYS_HH_MM_SS_MILISEC)")
    print("Ready.")

    // --- (4) Test Mandelbrot (not concurrent) ---------------------------------------------------------------------------------------------------------------------------------------------
    
    // --- (4.1) Specify the FileName
    fileName = "mandelbrotTest_SingleOptimized_1.tif"  // the filename to save
    
    // let stoppWatch:StoppWatch = StoppWatch()
    stoppWatch.start()
    let mandelbrot:Mandelbrot = Mandelbrot( bitmapWidth: bitmapWidth, bitmapHeight: bitmapHeight, minX:minX, maxX:maxX, minY:minY, maxY:maxY, maxSquareValue:maxSquareValue, maxIter:maxIter)
    let mandelbrotBitMap:BitMap = mandelbrot.calcMandelbrotBitMap_Single()
    stoppWatch.stop()
    
    // ... Saving the Bitmap .............................................................
    mandelbrotBitMap.saveBitMapAsTIFF( dir, toFileName: fileName )
    
    print("Finished creating and writing Mandelbrot Image to file \(fileName) in path: \(dir) to users home.\n\(mandelbrot)\nTime need for execution: \(stoppWatch.execTime) => \(stoppWatch.formattedExecTime_DAYS_HH_MM_SS_MILISEC)")
    print("Ready.")


    // --- (5) Test MyMandelbrot (concurrent) ---------------------------------------------------------------------------------------------------------------------------------------------
    
    // --- (5.1) Specify the FileName
    let numberOfConcurrentJobs = 4  // 16 // 8 // 6 // 4 // 2


    // --- (6) Test MyMandelbrot (concurrent lock free with gap at the borders) ---------------------------------------------------------------------------------------------------------------------------------------------
    
    // --- (6.1) Specify the FileName
    // let numberOfConcurrentJobs = 4
    fileName = "mandelbrotTest_Concurrent (no Locking).tif"  // the filename to save
    stoppWatch.start()
    let mandelbrot2:Mandelbrot = Mandelbrot( bitmapWidth: bitmapWidth, bitmapHeight: bitmapHeight, minX:minX, maxX:maxX, minY:minY, maxY:maxY, maxSquareValue:maxSquareValue, maxIter:maxIter)
    let mandelbrotBitMap2:BitMap = mandelbrot2.calcMandelbrotBitMap_Concurrent( numberOfConcurrentJobs)
    stoppWatch.stop()
    
    // ... Saving the Bitmap .............................................................
    mandelbrotBitMap2.saveBitMapAsTIFF( dir, toFileName: fileName )
    
    print("Finished creating and writing Mandelbrot Image to file \(fileName) in path: \(dir) to users home.\n\(mandelbrot2)\nTime need for execution: \(stoppWatch.execTime) => \(stoppWatch.formattedExecTime_DAYS_HH_MM_SS_MILISEC) ")
    print("Ready.")

    // --- (7) Test MyMandelbrot (concurrent lock free without gaps at the borders) ---------------------------------------------------------------------------------------------------------------------------------------------
    
    // --- (7.1) Specify the FileName
    // let numberOfConcurrentJobs = 4
    fileName = "mandelbrotTest_Concurrent_Overlapping_Sections.tif"  // the filename to save
    stoppWatch.start()
    let mandelbrot3:Mandelbrot = Mandelbrot( bitmapWidth: bitmapWidth, bitmapHeight: bitmapHeight, minX:minX, maxX:maxX, minY:minY, maxY:maxY, maxSquareValue:maxSquareValue, maxIter:maxIter)
    let mandelbrotBitMap3:BitMap = mandelbrot3.calcMandelbrotBitMap_Concurrent_Overlapping( numberOfConcurrentJobs, numberOfOverlappingLines: 1)
    stoppWatch.stop()
    
    // ... Saving the Bitmap .............................................................
    mandelbrotBitMap3.saveBitMapAsTIFF( dir, toFileName: fileName )
    
    print("Finished creating and writing Mandelbrot Image to file \(fileName) in path: \(dir) to users home.\n\(mandelbrot3)\nTime need for execution: \(stoppWatch.execTime) => \(stoppWatch.formattedExecTime_DAYS_HH_MM_SS_MILISEC) ")
    print("Ready.")

    
    // --- (8) Measue Scalability using concurrency ------
    
    let maxConcurrentJobs = 16
    print(" No of jobs | Filename | Directory | Execution Time")
    
    for var i in 1...maxConcurrentJobs {
        
        fileName = "mandelbrotBenchmark_Concurrent_" +  String(i) + ".tif"            // the filename to save
        stoppWatch.start()
        let mandelbrot:Mandelbrot = Mandelbrot( bitmapWidth: bitmapWidth, bitmapHeight: bitmapHeight, minX:minX, maxX:maxX, minY:minY, maxY:maxY, maxSquareValue:maxSquareValue, maxIter:maxIter)
        let mandelbrotBitMap2:BitMap = mandelbrot.calcMandelbrotBitMap_Concurrent( i)
        stoppWatch.stop()
        
        // ... Saving the Bitmap .............................................................
        mandelbrotBitMap2.saveBitMapAsTIFF( dir, toFileName: fileName )
        
        print("\(i) \t\t\t| \(fileName) \t| \(dir)\t| \(stoppWatch.execTime) => \(stoppWatch.formattedExecTime_DAYS_HH_MM_SS_MILISEC) ")
        
    }
    
    
} else {
    
    /// @ToDo: Error Handling
    print( "@ERROR: at MyMandelbrot.saveBitMapAsTIFF() user home does not have any diercories.")
}

