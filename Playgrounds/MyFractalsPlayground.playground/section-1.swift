// Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"


func saveBitMapAsTIFF( atDir:String?, toFile:String) {
    
    var theDir:String = ""
    
    if atDir == nil {
        if let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as? String {
            theDir = documentDirectory.stringByAppendingPathComponent("MyFolder")
        }
    } else {
        theDir = atDir!
    }
    
    println( theDir)
    
}


saveBitMapAsTIFF( nil, "fName.tif")


