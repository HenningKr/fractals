# README #

This repository contains the complementary source code for the **MyFractals** project on my Blog about *Programming OS X and iOS applications with Swift and Cocoa*.
You need to install Apple's development environment XCode and download the source code to your file system.
Open *Fractale.xcworkspace* from the project folder and all necessary source code, supporting files and configuration should be available.
A comprehensive description of the project is document on [my blog about Programming OS X and iOS applications with Swift and Cocoa](http://henning-hg.synology.me/wordpress//programming-applications-with-swift-and-cocoa-for-os-x-and-ios)

### What is this repository for? ###

# Quick summary
This repository contains the complementary source code for the *MyFractals* project on my Blog about Programming OS X and iOS applications with Swift and Cocoa.
The blog and complementary code cover the full project live cycle for an application including project creation, unit testing, functionality on BitMaps, file system, concurrent processing, User Interfaces and specifically on Fractals etc.

* Version
The source code and the blog is at a very early draft stage but will be further elaborated during the next weeks. Once application is completed version 1.0. will be commit to the repository.  

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Implement Xcode, download the project and open in Xcode *Fractale.xcworkspace* 

* Configuration
There is no other configuration required than distributed with the source code and supplementary files.

* Dependencies
There are no further dependencies than to Xcode 6.1, Max OS X 10.10 (Yosemite) and Swift and Cocoa framework  
 
* Database configuration
This code do not require any database set-up 

* How to run tests
Any tests implemented are explanatory and there isn't any complete test suite implemented.
Unit tests are based on the Xcode / Cocoa framework.

* Deployment instructions
Deployment instructions for the final package will be documented on the referenced blog and will be updated later one here.
 
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact